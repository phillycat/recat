install.packages(c("SPARQL"))
library(SPARQL)
library(survival)

#set generic properties
endpointUrl <- "http://localhost:8080/blazegraph/sparql"

#read sparql endpoint URL form environment variable
if(Sys.getenv("SPARQL_ENDPOINT")!="") {
  endpointUrl <- Sys.getenv("SPARQL_ENDPOINT")
}

query = 'prefix ncit: <http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix roo: <http://www.cancerdata.org/roo/>
prefix ro: <http://www.radiomics.org/RO/>
PREFIX uo: <http://purl.obolibrary.org/obo/UO_>

select ?patient ?age ?gender ?clinTStage ?clinNStage ?vitalStatus ?survivalMonths
where {
    ?patient rdf:type ncit:C16960.

    ?patient roo:P100000 ?ageDiagnosis.
    ?ageDiagnosis roo:P100027 ?ageUnitYears.
    ?ageUnitYears roo:P100042 ?age.

    ?patient roo:P100018 ?genderObj.
    ?genderObj rdf:type ?gender.
    FILTER( ?gender = ncit:C16576 || ?gender = ncit:C20197).

    ?patient roo:P100008 ?disease.
    ?disease roo:P100243 ?clinTNM.

    ?clinTNM roo:P100025 ?clinT.
    ?clinT rdf:type ncit:C48885.
    ?clinT rdf:type ?clinTStage.
    FILTER( ?clinTStage = ncit:C48720 ||
        ?clinTStage = ncit:C48724 ||
        ?clinTStage = ncit:C48728 ||
        ?clinTStage = ncit:C48732).

    ?clinTNM roo:P100025 ?clinN.
    ?clinN rdf:type ncit:C48884.
    ?clinN rdf:type ?clinNStage.
    FILTER( ?clinNStage = ncit:C48705 ||
        ?clinNStage = ncit:C48706 ||
        ?clinNStage = ncit:C48786 ||
        ?clinNStage = ncit:C48714).
    
    ?patient roo:P100028 ?vitalStatusObj.
    ?vitalStatusObj rdf:type ?vitalStatus.
    FILTER( ?vitalStatus = ncit:C28554 ||
        ?vitalStatus = ncit:C37987).
    ?vitalStatusObj roo:P100026 ?survivalObj.
    ?survivalObj roo:P100027 ?survivalMonthsObj.
    ?survivalMonthsObj rdf:type uo:0000035.
    ?survivalMonthsObj roo:P100042 ?survivalMonths.
}
ORDER BY ?patient'

myData <- SPARQL(endpointUrl, query)$results
myDataSub <- myData[myData$survivalMonths>=0,] # remove all patients with survival time < 0
rows <- nrow(myDataSub)

fileConn<-file("/output.txt")
writeLines(paste0("Rows: ", rows), fileConn)
close(fileConn)