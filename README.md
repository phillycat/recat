# reCAT

## Starting/stopping the infrastructure
To run this dataFAIRifier infrastructure, please execute the following line from the current folder.
On windows: Double-click start.bat
On linux, run the following command line:
```
sh start.bat
```

To stop the infrastructure:
On windows: Double-click stop.bat
On linux, run the following command line:
```
sh stop.bat
```

## Removing all data, and create containers from scratch
If you delete the containers, all processed data will be removed. Only the jupyter notebooks are kept/saved.

To remove all docker containers, please execute the following from the current folder:
On windows: double-click delete.bat
On linux, run the following command line:
```
sh delete.bat
```

Afterwards, you can use the default starting routine to start the infrastructure again.

# Updating the clinical data extraction

Go to [http://localhost:8888](http://localhost:8888) and open the jupyter notebook starting with "01_". Afterwards, continue with "02_*" etc. etc.

## DVH extraction

After launching, a folder called "dicom" is created. Please copy-paste the dicom images in here. This folder will be read by the DVH application.

### Finding the results of the DVH extraction

Afterwards, open [http://localhost:7200](http://localhost:7200) to see the SPARQL endpoint and be able to query it.
Example queries are available in the "queries" folder.